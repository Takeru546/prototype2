﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Actor : MonoBehaviour
{
    
    [HideInInspector]public HitPoints hitPoints;
    [HideInInspector] public Ragdoller ragdoller;
    [HideInInspector] public Rigidbody rb;
    [HideInInspector]public ParticleShooter pShooter;
    public ParticleSystem weaponParticle;

    [HideInInspector] public bool isDead;

    protected virtual void Start()
    {
        pShooter = GetComponent<ParticleShooter>();
        hitPoints = GetComponent<HitPoints>();
        ragdoller = GetComponent<Ragdoller>();
        rb = GetComponent<Rigidbody>();
    }

    protected virtual void Update()
    {
        
    }

    public virtual void ParticleShoot()
    {
        weaponParticle.Play();
    }

    public void TakeDamage(int damage)
    {
        hitPoints.Damage(damage);
    }
}
