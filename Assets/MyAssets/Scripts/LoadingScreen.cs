﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScreen : MonoBehaviour
{

    public Slider slider;

    void Start()
    {
        StartCoroutine(LoadNextScene(SceneLoader.leveltoload));
        GameState.gamePaused = false;
    }



    IEnumerator LoadNextScene(int sceneIndex)
    {
        AsyncOperation loading = SceneManager.LoadSceneAsync(sceneIndex);
        while (!loading.isDone)
        {
            float loadingProgress = Mathf.Clamp01(loading.progress / 0.9f);
            slider.value = loadingProgress;

            yield return null;
        }
    }
}
