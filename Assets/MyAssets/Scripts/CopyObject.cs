﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CopyObject : MonoBehaviour
{
    [HideInInspector] public int copyPowerID;
    [HideInInspector]public Collider col;
    private Rigidbody rb;

    public Transform axeModel;
    public float axeRotationSpeed = 10;

    private Transform originalParent;

    public float throwForce = 10;
    public float throwTurnSpeed = 50;


    public enum State {InHand, Stopped, Following, Returning, GoingForward};
    public State state = State.InHand;
    public bool inHand = true;

    private Transform targetToFollow;
    public Transform throwPos;

    private void Start()
    {
        col = GetComponent<Collider>();
        rb = GetComponent<Rigidbody>();
        rb.isKinematic = true;
        originalParent = transform.parent;
    }

    private void Update()
    {
        //Debug.Log("Estado do machado: " + state);
        switch (state)
        {
            case State.Following:
                FollowTarget(targetToFollow, throwForce, throwTurnSpeed);
                break;

            case State.InHand:
                ReturnToHand();
                break;

            case State.Returning:
                FollowTarget(originalParent, throwForce, throwTurnSpeed);
                break;

            case State.Stopped:
                StopMoving();
                break;

            case State.GoingForward:
                GoFoward();
                break;
        }

    }


    public void Throw(Transform target, State stateToGo)
    {
        transform.parent = null;
        transform.position = throwPos.position;
        transform.rotation = throwPos.rotation;
        targetToFollow = target;
        state = stateToGo;        
    }


    private void FollowTarget(Transform target, float force, float rotationForce)
    {
        rb.isKinematic = false;
        Vector3 direction = target.position - rb.position;
        direction.Normalize();
        Vector3 rotationAmount = Vector3.Cross(transform.forward, direction);
        rb.angularVelocity = rotationAmount * rotationForce;
        rb.velocity = direction * force;
        SpinAxe();
    }

    private void GoFoward()
    {
        inHand = false;
        rb.isKinematic = false;
        rb.velocity = transform.forward * throwForce;
        rb.angularVelocity = new Vector3(0, 0, 0);
        SpinAxe();
    }

    public void ReturnToHand()
    {
        if(!inHand)
        {
            transform.parent = originalParent;
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.Euler(0, 0, 0);
            axeModel.transform.localPosition = Vector3.zero;
            axeModel.localRotation = Quaternion.Euler(0, 0, 0);
            //melhorar essa parte pq é o msm codigo do StopMoving
            rb.isKinematic = true;
            rb.angularVelocity = new Vector3(0, 0, 0);
            rb.velocity = Vector3.zero;
            inHand = true;
        }

    }

    private void StopMoving()
    {
        rb.isKinematic = true;
        rb.angularVelocity = new Vector3(0, 0, 0);
        rb.velocity = Vector3.zero;
    }

    private void OnTriggerEnter(Collider other)
    {
        //Pega o poder
        var enemyActor = other.GetComponentInParent<StateController>();
        if (!enemyActor)
        {
            enemyActor = other.GetComponent<StateController>();
        }
        if (enemyActor)
        {
            copyPowerID = enemyActor.troopData.powerId;
        }
        if (state == State.Following || state == State.GoingForward) //p não bater na mão
        {            
            state = State.Stopped;
        }
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.GetComponentInParent<PlayerActor>() && state == State.Returning)
        {
            state = State.InHand;
        }
    }

    public void SpinAxe()
    {
        axeModel.Rotate(axeRotationSpeed * Time.deltaTime, 0, 0);
    }
   
}
