﻿using ECM.Common;
using ECM.Controllers;
using UnityEngine;
using Cinemachine;
using UnityEngine.InputSystem;

namespace ECM.Walkthrough.MovementRelativeToCamera
{
    /// <summary>
    /// Custom character controller. This shows how make the character move relative to MainCamera view direction.
    /// </summary>

    public class MyCharacterController : BaseCharacterController
    {

        public Camera mainCamera;
        public Transform camFollowTarget;

        //[HideInInspector]public PlayerInputActions inputActions;
        [HideInInspector] public PlayerInput inputActions;
        public CinemachineFreeLook cinemachineFreeLook;

        public override void Awake()
        {
            base.Awake();
            inputActions = new PlayerInput();
            inputActions.Enable();

        }
        private void OnEnable()
        {
            inputActions.Enable();
        }

        private void OnDisable()
        {
            inputActions.Disable();

        }

        protected override void Animate()
        {
            if (moveDirection == Vector3.zero)
                animator.SetBool("isWalking", false);
            else
            {
                animator.SetBool("isWalking", true);
                animator.SetFloat("runX", Mathf.Round(inputActions.Player.Move.ReadValue<Vector2>().x));
                animator.SetFloat("runY", Mathf.Round(inputActions.Player.Move.ReadValue<Vector2>().y));
            }
        }

        protected override void UpdateRotation()
        {
            RotateTowards(camFollowTarget.forward, true);
        }

        protected override void HandleInput()
        {
            // Toggle pause / resume.
            // By default, will restore character's velocity on resume (eg: restoreVelocityOnResume = true)

            //if (Input.GetKeyDown(KeyCode.P))
            //    pause = !pause;

            //// Handle user input

            //jump = inputActions.Player.Jump.ReadValue<bool>();

            //crouch = inputActions.Player.Crouch.ReadValue<bool>();

            moveDirection = new Vector3
            {
                x = inputActions.Player.Move.ReadValue<Vector2>().x,
                y = 0.0f,
                z = inputActions.Player.Move.ReadValue<Vector2>().y
            };

            // Transform the given moveDirection to be relative to the main camera's view direction.
            // Here we use the included extension .relativeTo...


            if (mainCamera != null)
                moveDirection = moveDirection.relativeTo(mainCamera.transform);
        }

        public void HandleJumpInput(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                jump = true;
                animator.SetTrigger("jump");
            }
            else if (context.canceled)
                jump = false;

        }

        public void HandleCrouchInput(InputAction.CallbackContext context)
        {
            if (context.started)
            {
                crouch = true;
                animator.SetBool("isCrouching", true);

            }
            else if (context.canceled)
            {
                crouch = false;
                animator.SetBool("isCrouching", false);
            }


        }

    }
}
