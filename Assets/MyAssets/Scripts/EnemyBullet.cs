﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{

    [HideInInspector]public int damage = 1;
    [SerializeField] float baseLifeTime = 5f;
    private float lifeTime;



    private void Start()
    {
        lifeTime = baseLifeTime;
    }

    private void OnEnable()
    {
        lifeTime = baseLifeTime;
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0)
        {
            SelfDestruct();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            //BatMode hasBatmode = other.GetComponent<BatMode>();

            //if (hasBatmode)
            //{
            //    if (hasBatmode.isInBatMode)
            //    {
            //        other.GetComponent<BloodPoints>().Damage(damage);
            //    }
            //    else
            //        other.GetComponent<HitPoints>().Damage(damage);
            //}
            /*else*/
            if (other.GetComponent<HitPoints>())
                other.GetComponent<HitPoints>().Damage(damage);
            SelfDestruct();
        }

        //else if (other.gameObject.tag != "Enemy" && !other.gameObject.GetComponent<CheckLevel2Done>()  && !other.gameObject.GetComponent<EnemyArea>())
        //{
        //    SelfDestruct();
        //}

    }

    private void SelfDestruct()
    {
        ObjectPooler.SharedInstance.PoolDestroy(gameObject);
    }
}
