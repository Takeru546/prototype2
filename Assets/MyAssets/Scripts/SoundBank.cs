﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "SoundBank_", menuName = "SoundBank")]
public class SoundBank : ScriptableObject
{
    public SoundData step;
    [Range(-3f, 3f)]
    public float stepPitchMin;
    [Range(-3f, 3f)]
    public float stepPitchMax;

    public SoundData takeHit;

    public SoundData throwAxe;
    public SoundData shoot;

    public SoundData dash;
    public SoundData death;

    public SoundData meleeAttack;
    public SoundData rangedAttack;
}
