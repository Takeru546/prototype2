﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatStarter : MonoBehaviour
{
    public StateController[] enemies;

    private void Start()
    {
        enemies = GetComponentsInChildren<StateController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            foreach (var enemy in enemies)
            {
                enemy.StartCombat();
            }
        }
    }
}
