﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeHand : MonoBehaviour
{

    [HideInInspector] public int damageToDo;
    [HideInInspector] public bool attackOn = false;
    [HideInInspector] public string target;
    [HideInInspector] public BoxCollider hitbox;
    public List<GameObject> damagedObjs = new List<GameObject>();
    private bool isColliding;

    public int enemiesDamaged;
    public GameObject enemyHitVfx;
    public GameObject playerHitVfx;
    public GameObject bloodAttackHitVfx;
    public GameObject spawnerHitVfx;
    public int knockBackForce = 2500;

    public delegate void OnDamageDealt();
    public event OnDamageDealt onDamageDealt;

    private void Awake()
    {
        hitbox = GetComponent<BoxCollider>();
        hitbox.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(attackOn && other.tag != gameObject.tag && other.GetComponent<HitPoints>() && !damagedObjs.Contains(other.gameObject)  
            || attackOn && other.tag != gameObject.tag && other.GetComponentInParent<TroopDeath>() && !damagedObjs.Contains(other.gameObject))
        {
            //BatMode hasBatmode = other.GetComponent<BatMode>();
            damagedObjs.Add(other.gameObject);
           // if (hasBatmode)
           // {
           //     if (hasBatmode.isInBatMode)
           //     {
           //         other.GetComponent<BloodPoints>().Damage(damageToDo);
           //     }
           //     else
           //         other.GetComponent<HitPoints>().Damage(damageToDo);
           // }
           // else
            {
                if(GetComponent<HitPoints>())
                other.GetComponent<HitPoints>().Damage(damageToDo);
                else
                    other.GetComponentInParent<HitPoints>().Damage(damageToDo);

            }


            onDamageDealt?.Invoke();
            
            //vfxs
          //if (gameObject.name == "PlayerMeleeHand" && !other.GetComponent<EnemySpawner>())
          //{
          //    Instantiate(enemyHitVfx, transform.position, transform.rotation);
          //    if(other.GetComponent<Rigidbody>())
          //    other.GetComponent<Rigidbody>().AddForce(transform.forward * knockBackForce);
          //}
          //
          //else if(gameObject.name == "PlayerBloodHand" && !other.GetComponent<EnemySpawner>())
          //{
          //    Instantiate(bloodAttackHitVfx, other.transform.position + (Vector3.up * 1.5f), transform.rotation);
          //
          //}
          //else if(gameObject.name == "PlayerBloodHand" || gameObject.name == "PlayerMeleeHand" && other.GetComponent<EnemySpawner>())
          //{
          //    if(spawnerHitVfx)
          //    Instantiate(spawnerHitVfx, other.transform.position , other.transform.rotation);
          //
          //}
          //  else
                Instantiate(playerHitVfx, transform.position, transform.rotation);

            enemiesDamaged++;
        }
    }



    private void OnDrawGizmos()
    {
        if(attackOn)
        {
            Color gizmosColor = Color.red;
            gizmosColor.a = 0.7f;
            Gizmos.color = gizmosColor;
            Vector3 gizmosSize = hitbox.size;

            Gizmos.DrawCube(transform.position, gizmosSize);
        }

    }
}
