﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameState : MonoBehaviour
{

    public static bool gamePaused = false;
    public GameObject player;
    public GameObject deathPanel;
    [SerializeField] private float normalTimeScale = 1;
    public GameObject pausePanel;
    //private bool done = true;


    void Update()
    {
        if (SceneManager.GetActiveScene().buildIndex == 0 || SceneManager.GetActiveScene().buildIndex == 1)
        {
            Cursor.lockState = CursorLockMode.None;
            Time.timeScale = normalTimeScale;
            Cursor.visible = true;
        }
        else if (gamePaused)
        {
            Time.timeScale = 0;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;

        }
        else if (!gamePaused)
        {
            Time.timeScale = normalTimeScale;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

        }

        if (player.GetComponent<HitPoints>().currentPoints <= 0)
        {
            PlayerDead();
        }

        //if (/*Input.GetKeyDown(KeyCode.Escape) &&*/ SceneManager.GetActiveScene().buildIndex != 0 || SceneManager.GetActiveScene().buildIndex != 1) //pausa com esq qdo n ta em loading ou no menu
        //{
        //    gamePaused = !gamePaused;
        //    done = false;
        //}else if (gamePaused && !done)
        //{
        //    PauseGame();
        //}
        //else if (!gamePaused && !done)
        //{
        //    ResumeGame();
        //}
    }


    public void CheckPauseInput()
    {
        if (SceneManager.GetActiveScene().buildIndex > 1)
        {
            if (!gamePaused)
            {
                PauseGame();
            }
            else
                ResumeGame();
        }
    }

    public void PauseGame()
    {
        gamePaused = true;
        pausePanel.SetActive(true);
        //done = true;
    }

    public void ResumeGame()
    {
        gamePaused = false;
        pausePanel.SetActive(false);
        //done = true;
    }

    public void PlayerDead()
    {
        deathPanel.SetActive(true);
        gamePaused = true;
    }
}

