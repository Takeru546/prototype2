﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileShooting : MonoBehaviour
{

    public ObjectPooler bulletPooler;

    private void Start()
    {
       // if (GetComponentInParent<EnemySpawner>())
       // {
       //     bulletPooler = transform.parent.GetComponentInChildren<ObjectPooler>();
       // }
    }

    public GameObject ShootBullet(Vector3 shotPos, Quaternion rotation)
    {
        GameObject bulletInstance = bulletPooler.PoolInstantiate(shotPos, rotation);
        return bulletInstance;
    }
}
