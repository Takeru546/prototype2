﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBulletLauncher : MonoBehaviour
{
    private Rigidbody rb;
    [HideInInspector]public float force = 1;




    private void Awake()
    {
        rb = GetComponent<Rigidbody>();

    }


    private void OnEnable()
    {
        rb.isKinematic = false;
        rb.velocity = transform.forward * force;

    }

    private void OnDisable()
    {
        rb.isKinematic = true;
    }

    private void Update()
    {
        if(gameObject.activeInHierarchy)
        {
            GoFoward();
        }
    }

    void GoFoward()
    {
        rb.velocity = transform.forward * force;
    }

    private void SelfDestruct()
    {
        ObjectPooler.SharedInstance.PoolDestroy(gameObject);
    }
}
