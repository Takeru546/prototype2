﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu (fileName = "Action_", menuName = "Troop/Actions/AllowHitstun")]
public class AllowHitstunAction : Action
{
    public bool allowHitstun;

    public override void Act(StateController controller)
    {
        //controller.allowHitstun = allowHitstun;
    }
}
