﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Troop/Actions/Chase")]
public class ChaseAction : Action
{
    public override void Act(StateController controller)
    {
        //RotateTowards(controller);
        Chase(controller);
    }

    private void Chase(StateController controller)
    {
        controller.navMeshAgent.isStopped = false;
        controller.navMeshAgent.destination = controller.target.position;
    }

    private void RotateTowards(StateController controller)
    {
        Vector3 direction = (controller.target.position - controller.transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        controller.transform.rotation = Quaternion.Slerp(controller.transform.rotation, lookRotation, Time.deltaTime * 15f);

    }
}
