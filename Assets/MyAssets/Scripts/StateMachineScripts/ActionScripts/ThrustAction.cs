﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Troop/Actions/Thrust")]
public class ThrustAction : Action
{
    public float thrustSpeed = 5;

    public override void Act(StateController controller)
    {
        Thrust(controller);
    }

    private void Thrust(StateController controller)
    {
        if (!controller.endForce)
        {
            controller.navMeshAgent.isStopped = true;
            controller.rb.isKinematic = false;
            controller.rb.constraints = RigidbodyConstraints.FreezeRotation;
            controller.rb.AddForce(controller.transform.forward * thrustSpeed, ForceMode.Impulse);
            Debug.Log("Force: " + thrustSpeed);
        }

        else
        {
            controller.rb.isKinematic = true;
            Debug.Log("EndThrust");
        }
    }
}
