﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Troop/Actions/ThrustRecover")]
public class ThrustRecoverAction : Action
{
    public override void Act(StateController controller)
    {
        controller.rb.isKinematic = true;
    }
}
