﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[CreateAssetMenu(menuName = "Troop/Actions/Dash")]
public class DashAction : Action
{
    public Vector3 destination;
    public float dashDuration;

    public override void OnStateEnterAction(StateController controller)
    {
        Dash(controller);
    }

    private void Dash(StateController controller)
    {
        controller.rb.DOMove(controller.transform.position + destination, dashDuration);
    }
}
