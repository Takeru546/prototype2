﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Action_", menuName = "Troop/Actions/ParticleShoot")]
public class ParticleShootAction : Action
{
    private float nextFireTime;

    public override void Act(StateController controller)
    {
        Fire(controller);
    }


    public void Fire(StateController controller)
    {
        controller.enemyShooting.FireParticle(controller.weaponParticle, controller.troopData.attackRate);
    }
}
