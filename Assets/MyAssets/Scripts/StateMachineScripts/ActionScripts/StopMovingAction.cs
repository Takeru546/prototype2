﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Action_", menuName = "Troop/Actions/StopMoving")]
public class StopMovingAction : Action
{
    public bool lockMovement = true;
    public bool lockRotation = true;

    public override void Act(StateController controller)
    {
        StopMovement(controller);
    }

    private void StopMovement(StateController controller)
    {
        controller.navMeshAgent.isStopped = true;

        if(lockMovement)
        {
            controller.transform.position = controller.transform.position;
            if(controller.rb)
            {
                controller.rb.isKinematic = true;
            }

        }
            

        if (lockRotation)
            controller.transform.rotation = controller.transform.rotation;
    }
}
