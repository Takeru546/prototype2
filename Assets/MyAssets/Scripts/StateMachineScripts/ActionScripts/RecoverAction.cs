﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Action_", menuName = "Troop/Actions/Recover")]
public class RecoverAction : Action
{
    public override void Act(StateController controller)
    {
        Recover(controller);
    }

    private static void Recover(StateController controller)
    {
        controller.currentAnimation = "Recovering";
        controller.animator.SetTrigger("recover");
    }
}
