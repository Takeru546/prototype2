﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Action_", menuName = "Troop/Actions/ProjectileShoot")]
public class ProjectileShootAction : Action
{
    public override void Act(StateController controller)
    {
        Shoot(controller);
    }

    private static void Shoot(StateController controller)
    {
        controller.navMeshAgent.isStopped = true;
        controller.ShootProjectiles();    
    }
}
