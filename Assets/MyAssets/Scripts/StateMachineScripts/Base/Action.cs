﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Action : ScriptableObject
{
    public virtual void Act(StateController controller)
    {

    }

    public virtual void OnStateEnterAction(StateController controller)
    {
        Debug.Log("Action State Enter");
    }

    public virtual void OnStateExitAction(StateController controller)
    {
        Debug.Log("Action State Exit");
    }
}
