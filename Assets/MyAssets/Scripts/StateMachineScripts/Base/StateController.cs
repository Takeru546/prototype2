﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class StateController : Actor
{

    public State currentState;
    public State remainState;
    public State startingState;
    public State outOfCombatState;
    public State deadState;

    public TroopData troopData;
    public SoundBank soundBank;
    public Transform[] eyes = new Transform[1];
    public float freezeTime = 1f;

    [HideInInspector] public NavMeshAgent navMeshAgent;
    [HideInInspector] public EnemyShooting enemyShooting;
    [HideInInspector] public Animator animator;
    [HideInInspector] public AudioSource audioSource;
    [HideInInspector] public TroopDeath troopDeath;

    [HideInInspector] public Transform chaseTarget;
    [HideInInspector] public float stateTimeElapsed;
    [HideInInspector] public float thrustSpeed = 5;
    public Transform target;

    [HideInInspector] public string currentAnimation;
    [HideInInspector] public string currentAnimationTrigger;

    [HideInInspector] public bool endForce = false;


    [SerializeField] private bool aiActive = true;

    void Awake()
    {
        enemyShooting = GetComponent<EnemyShooting>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshAgent.speed = troopData.movementSpeed;
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        hitPoints = GetComponent<HitPoints>();
        hitPoints.basePoints = troopData.baseHP;
        troopDeath = GetComponent<TroopDeath>();
        currentState = outOfCombatState;
    }

    protected override void Start()
    {
        base.Start();
        OnEnable();
    }

    private void OnEnable()
    {
        StartCoroutine(UpdateStates());
    }

    private void OnDisable()
    {
    }


    protected override void Update()
    {
        base.Update();
        troopDeath.CheckHP(hitPoints);

        
    }

    public IEnumerator UpdateStates()
    {
        while (true)
        {
            if (!aiActive)
                yield return new WaitForSeconds(.1f);
            else
            {
                currentState.UpdateState(this);
                Debug.Log("Updating State");
                yield return new WaitForSeconds(.1f);
            }

        }

    }

    public void TransitionToState(State nextState)
    {
        if (nextState != remainState)
        {
            ChangeState(nextState);
            
            currentState = nextState;
            

            OnExitState();
        }
    }

    private void OnExitState()
    {
        ResetCountDown();
    }

    public void ChangeState(State stateToChange)
    {
        currentState.OnStateExit(this);
        currentState = stateToChange;
        currentState.OnStateEnter(this);
    }

    public void StartCombat()
    {
        ChangeState(startingState);
        if (hitPoints.slider != null)
        {
            hitPoints.slider.gameObject.SetActive(true);
        }
    }


    public bool CheckIfCountDownElapsed(float duration)
    {
        stateTimeElapsed += Time.deltaTime;
        return stateTimeElapsed >= duration;
    }

    private void ResetCountDown()
    {
        stateTimeElapsed = 0;
    }

    public void StartForce()
    {
        endForce = false;
    }

    public void EndForce()
    {
        endForce = true;
    }



    public void ShootProjectiles()
    {
        enemyShooting.FireProjectile(eyes, troopData.attackRate, troopData.attackDamage, troopData.attackForce);
    }

    public void Dead()
    {
        HpSliderOff();
        ChangeState(deadState);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = currentState.sceneGizmoColor;
        Gizmos.DrawWireSphere(transform.position, troopData.closeAreaRange);
        Gizmos.DrawWireSphere(transform.position, troopData.farAreaRange);
    }

    public void HpSliderOff()
    {
        if (hitPoints.slider)
            hitPoints.slider.gameObject.SetActive(false);
    }
}
