﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Troop/Decisions/CheckIfCloseDecision")]
public class CheckIfCloseDecision : Decision
{
    public bool isCloseArea = true;

    public override bool Decide(StateController controller)
    {
        return CheckIfClose(controller);
    }

    private bool CheckIfClose(StateController controller)
    {
        float targetDistace = Vector3.Distance(controller.transform.position, controller.target.position);

        if(isCloseArea)
        {
            if (targetDistace < controller.troopData.closeAreaRange)
            {
                return true;
            }
            else return false;
        }

        else
        {
            if (targetDistace < controller.troopData.farAreaRange)
            {
                return true;
            }
            else return false;
        }

    }
}
