﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Decision_", menuName = "Troop/Decisions/CountDown")]
public class CountDownDecision : Decision
{

    public float minTimer;
    public float maxTimer;
    private float timer;

    public override void OnStateEnterDecisionActions()
    {
        base.OnStateEnterDecisionActions();

        if(minTimer > maxTimer)
        {
            minTimer = maxTimer;
        }

        timer = Random.Range(minTimer, maxTimer);
        Debug.Log("Timer generated: " + timer);
    }

    public override bool Decide(StateController controller)
    {
        bool isCountDownFinished = controller.CheckIfCountDownElapsed(timer);
        return isCountDownFinished;
    }


}
