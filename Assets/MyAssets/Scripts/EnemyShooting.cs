﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour
{

    [HideInInspector] public ProjectileShooting projectileShooting;
    public SoundBank soundBank;
    private AudioSource audioSource;
    private float nextFireTime = .1f;


    void Awake()
    {
        projectileShooting = GetComponent<ProjectileShooting>();
        audioSource = GetComponent<AudioSource>();
    }

    public void FireProjectile(Transform[] shotPos, float fireRate, int damage, float attackForce)
    {
        if(CheckTimerContinuous(fireRate))
        {
            for (int i = 0; i < shotPos.Length; i++)
            {
                FireSingleProjectile(shotPos[i], damage, attackForce);
            }

        }
    }

    public void FireSingleProjectile(Transform shotPos, int damage, float attackForce)
    {
        GameObject bulletInstance = projectileShooting.ShootBullet(shotPos.position, shotPos.rotation);
        bulletInstance.GetComponent<EnemyBullet>().damage = damage;
        bulletInstance.GetComponent<ProjectileBulletLauncher>().force = attackForce;
    }

    public void FireParticle(ParticleSystem pShooter, float attackRate)
    {
        if(CheckTimerContinuous(attackRate))
        {
            pShooter.Play();
        }
    }

    public bool CheckTimerContinuous(float timeToAdd)
    {
        if (Time.time > nextFireTime)
        {
            nextFireTime = Time.time + timeToAdd;
            return true;
        }
        else return false;
    }



}
