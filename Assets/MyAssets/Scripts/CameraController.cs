﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CameraController : MonoBehaviour
{

    private PlayerActor playerActor;
    public float sensitivityX = 1;
    public float sensitivityY = 1;
    public float maxYAngle = 80f;
    private Vector2 currentRotation;


    void Start()
    {
        playerActor = GetComponentInParent<PlayerActor>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        UpdateCameraRotation();
    }

    private void UpdateCameraRotation()
    {
        currentRotation.x += playerActor.controller.inputActions.Player.Look.ReadValue<Vector2>().x * sensitivityX;
        currentRotation.y -= playerActor.controller.inputActions.Player.Look.ReadValue<Vector2>().y * sensitivityY;
        currentRotation.x = Mathf.Repeat(currentRotation.x, 360);
        currentRotation.y = Mathf.Clamp(currentRotation.y, -maxYAngle, maxYAngle);
        transform.rotation = Quaternion.Euler(currentRotation.y, currentRotation.x, 0);

    }
}
