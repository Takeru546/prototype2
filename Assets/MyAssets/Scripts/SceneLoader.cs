﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public static int leveltoload;


    public void LoadLevel(int lvl)
    {
        leveltoload = lvl;
        SceneManager.LoadScene(1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ReloadCurrentScene()
    {
        LoadLevel(SceneManager.GetActiveScene().buildIndex);
    }

    public void LoadNextScene()
    {
        int currentSceneInd = SceneManager.GetActiveScene().buildIndex;
        LoadLevel(currentSceneInd + 1);
    }


}
