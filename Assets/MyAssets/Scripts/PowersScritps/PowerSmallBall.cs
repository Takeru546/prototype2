﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Powers/ SmallBall")]
public class PowerSmallBall : Power
{
    public override void Execute(Actor actor)
    {
        actor.ParticleShoot();
    }
}
