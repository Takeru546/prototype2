﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Power_Copy", menuName = "Powers/ PowerCoy")]
public class PowerCopy : Power
{
    public override void Execute(Actor actor)
    {
        ((PlayerActor)actor).StartCoroutine(((PlayerActor)actor).ExecuteCopy());
    }


}
