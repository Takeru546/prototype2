﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Power : ScriptableObject
{
    public string powerName;
    public int id;
    public int damage = 1;
    public int powerAmmo = 10;


    public abstract void Execute(Actor actor);
    
}
