﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleCollisionListener : MonoBehaviour
{
    private ParticleSystem part;
    private List<ParticleCollisionEvent> collisionEvents;
    public int damage = 1;
    public bool doRagdoll = false;
    public float ragdollForce = 30f;
    public int powerId;

    void Start()
    {
        int.TryParse(name.Substring(0, 2), out powerId);

        
        part = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
    }

    private void OnParticleCollision(GameObject other)
    {        
        int numCollisionEvents = part.GetCollisionEvents(other, collisionEvents); //Obsolete?
        for (int i = 0; i < numCollisionEvents; i++)
        {
            var actor = other.GetComponentInParent<Actor>();
            if (actor == null)
                actor = other.GetComponent<Actor>();
            if (actor == null)
                return;
            if((doRagdoll && actor.ragdoller) || actor.isDead && actor.ragdoller)
            {
                actor.ragdoller.DoRagdoll(true);
                other.GetComponent<Rigidbody>().AddForce(collisionEvents[i].intersection * ragdollForce, ForceMode.Impulse);
            }

            if (actor.GetComponent<PowerPoints>() && actor.GetComponent<PowerPoints>().slider.isActiveAndEnabled)
            {
                actor.GetComponent<PowerPoints>().Damage(damage);
            }else
                actor.hitPoints.Damage(damage);

        }
    }

}
