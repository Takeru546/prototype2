﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.InputSystem;
using ECM.Walkthrough.MovementRelativeToCamera;
using SensorToolkit;

public class PlayerActor : Actor
{
    

    [HideInInspector] public MyCharacterController controller;
    [HideInInspector] public CopyObject copyObject;

    public delegate void OnPowerChange();
    public event OnPowerChange onPowerChange;


    public List<Power> powerList;
    public List<GameObject> powerVFXList;
    

    public Power currentPower;
    public int currentAmmo;
    public Transform shootPos;


    protected override void Start()
    {
        base.Start();

        //Carrega os vfx da pasta
        var particles = Resources.LoadAll("PowersVFX", typeof(GameObject)).Cast<GameObject>();
        foreach (var go in particles)
        {
            powerVFXList.Add(Instantiate(go, transform));
        }

        //Carrega os poderes da pasta
        var powers = Resources.LoadAll("ScriptableObjects/Powers", typeof(Power)).Cast<Power>();
        foreach (var go in powers)
        {
            int.TryParse(go.name.Substring(0, 2), out go.id);
            powerList.Add(go);
        }

        
        controller = GetComponent<MyCharacterController>();
        copyObject = GetComponentInChildren<CopyObject>();


        currentPower = powerList.Find(x => x.id == 0); //Seta o copy power como current power
        currentAmmo = currentPower.powerAmmo;

    }

    private void OnEnable()
    {
        onPowerChange += UpdateWeaponParticle;
        onPowerChange += FillAmmo;
    }

    private void OnDisable()
    {
        onPowerChange -= UpdateWeaponParticle;
        onPowerChange -= FillAmmo;
    }

    protected override void Update()
    {
        base.Update();
        if(copyObject.copyPowerID != 0 && currentAmmo == 0)
        {
            currentPower = powerList.Find(x => x.id == 0);
            copyObject.copyPowerID = 0;
            FillAmmo();
        }
    }



    public void ExecutePower(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            currentPower.Execute(this);
        }
    }

    public IEnumerator ExecuteCopy()
    {
        var cam = controller.mainCamera.GetComponent<RaySensor>();
        var targets = cam.DetectedObjects; //n tá usando isso esse targets p nd???
        if (copyObject.state == CopyObject.State.InHand && copyObject.copyPowerID == 0) // só joga se tiver o poder de copiar e na mao
        {
            controller.animator.SetTrigger("throw");
            foreach (var item in cam.DetectedObjects)
            {
                if(item.CompareTag("Enemy"))
                {
                    copyObject.Throw(item.transform, CopyObject.State.Following);
                    yield return null;
                }
                copyObject.Throw(item.transform, CopyObject.State.GoingForward);
            }            
        }
            

        else if (copyObject.state != CopyObject.State.InHand)
            copyObject.state = CopyObject.State.Returning; // se apertar o botão com o machado fora da mão

        yield return new WaitForSeconds(.1f); // Só para testar


        if (copyObject.copyPowerID != 0 && copyObject.state == CopyObject.State.InHand)
        {
            currentPower = powerList.Find(x => x.id == copyObject.copyPowerID);
            onPowerChange?.Invoke();  // se deixar assim ele só muda o poder qdo clica dnv?
        }
        yield return null;
    }

    public override void ParticleShoot()
    {
        var cam = controller.mainCamera.GetComponent<RaySensor>();
        var targets = cam.DetectedObjects;
        RaycastHit hit = new RaycastHit();
        if (targets != null && currentAmmo > 0)
        {
            hit = cam.GetRayHit(targets[0]);

            weaponParticle.transform.LookAt(hit.point); //Gira a weaponParticle pra direção da mira
            weaponParticle.Play();
        }
            currentAmmo--;
        
    }

    private void UpdateWeaponParticle()
    {
        weaponParticle = powerVFXList.Find(x => x.GetComponent<ParticleCollisionListener>().powerId == currentPower.id).GetComponent<ParticleSystem>();
        weaponParticle.transform.position = shootPos.position; //p particula ficar um pouco na frente do player e n bater no collider
    }

    private void FillAmmo()
    {
        currentAmmo = currentPower.powerAmmo;
    }
}
