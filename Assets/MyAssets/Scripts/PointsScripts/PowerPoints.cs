﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerPoints : Points
{
   [HideInInspector] public PlayerActor playerActor;

    public override event OnDamage onDamage;

    public override void Start()
    {
        playerActor = GetComponent<PlayerActor>();
    }


    // Update is called once per frame
    public override void Update()
    {
        UpdateSlider();
        LimitPoints();
        if (playerActor.currentPower.id == 0)
        {
            slider.gameObject.SetActive(false);
        }
        else
            slider.gameObject.SetActive(true);
        basePoints = playerActor.currentPower.powerAmmo;
        currentPoints = playerActor.currentAmmo;
    }


    public override void Damage(int damageToDeal)
    {
        playerActor.currentAmmo -= damageToDeal;
        onDamage?.Invoke();

    }
}
