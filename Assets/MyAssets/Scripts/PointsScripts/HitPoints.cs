﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HitPoints : Points
{
    [HideInInspector] public bool invulnerable = false;
    private bool runLastTimeHittedTimer;
    public float lastTimeHitted;
    public float timeBetweenHitAllow = 0;
    public GameObject lowLifeFeedback;
    public override event OnDamage onDamage;
    private bool isPlayer;

    public override void Start()
    {
        currentPoints = basePoints;
        lastTimeHitted = timeBetweenHitAllow + 1f;
        if (slider != null)
        {
            slider.maxValue = basePoints;
        }
        isPlayer = GetComponent<PlayerActor>();
    }

    public override void Update()
    {
        UpdateSlider();
        LimitPoints();

        if (runLastTimeHittedTimer)
        {
            lastTimeHitted += Time.deltaTime;
        }


        if (isPlayer)
        {
            if (lastTimeHitted > 10f)
            {
                Heal(basePoints - currentPoints);
            }

            if (currentPoints < 3 && currentPoints > 0)
            {
                lowLifeFeedback.SetActive(true);
            }
            else
                lowLifeFeedback.SetActive(false);
        }
        if (!isPlayer)
        {
            if(currentPoints < ((float)basePoints /100)*50)
            {
                ShowSlider();
            }else if (currentPoints > ((float)basePoints / 100) * 50 && lastTimeHitted > 2f)
            {
                HideSlider();
            }
        }
    }

    public override void Damage(int damageToDeal)
    {
        if (!invulnerable && lastTimeHitted > timeBetweenHitAllow)
        {
            if (!isPlayer)
                ShowSlider();
            StartTimer();
            currentPoints -= damageToDeal;
            onDamage?.Invoke();

        }
    }

    public void StartTimer()
    {
        runLastTimeHittedTimer = true;
        lastTimeHitted = 0;
    }

    public void ShowSlider()
    {
        slider.gameObject.SetActive(true);
    }

    public void HideSlider()
    {
        slider.gameObject.SetActive(false);

    }
}
