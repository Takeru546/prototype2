﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Points : MonoBehaviour
{

    public Slider slider;

    public int basePoints;
    /*[HideInInspector]*/ public int currentPoints;

    public delegate void OnDamage();
    public virtual event OnDamage onDamage;

    public delegate void OnHeal();
    public event OnHeal onHeal;

    public virtual void Start()
    {
        currentPoints = basePoints;
        if (slider != null)
        {
            slider.maxValue = basePoints;
        }
    }

    public virtual void Update()
    {
        UpdateSlider();
        LimitPoints();
    }

    public void UpdateSlider()
    {
        if (slider != null)
        {
            slider.value = currentPoints;
            slider.maxValue = basePoints;
        }
    }

    public void LimitPoints()
    {
        if(currentPoints > basePoints)
        {
            currentPoints = basePoints;
        }

        if (currentPoints < 0)
        {
            currentPoints = 0;
        }
    }

    private void OnEnable()
    {
        currentPoints = basePoints;
    }

    public virtual void Damage(int damageToDeal)
    {

        currentPoints -= damageToDeal;
        onDamage?.Invoke();

    }

    public void Heal(int healAmount)
    {
        currentPoints += healAmount;
        onHeal?.Invoke();
    }

}
