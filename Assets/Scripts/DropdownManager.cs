﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropdownManager : MonoBehaviour
{
    [SerializeField] private HUDSettings settings;

    public TMPro.TMP_Dropdown dropdown;
    public DropdownType myType;

    public void GetDropdownValue()
    {
        settings.ChangeValue(dropdown.value, myType);
    }

}
