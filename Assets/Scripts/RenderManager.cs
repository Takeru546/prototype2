﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.Rendering;

public static class RenderSettings
{
    public static Quality shadowQuality;
    public static Enabled fog;
    public static Enabled motionBlur;
    public static Enabled depthOfField;
    public static Enabled ambientOcclusion;
    public static AntiAliasing antiAliasing;
    public static Enabled shadows;
}

#region ENUMS

public enum Quality
{
    LOW,
    MEDIUM,
    HIGH,

}

public enum Enabled
{
    DISABLED,
    ENABLED
}

public enum AntiAliasing
{
    FXAA,
    SMAA,
    TAA,
    DISABLED
}

#endregion

public class RenderManager : MonoBehaviour
{
    [Header("Editor Data")]
    [SerializeField] private RenderConfig config;

    [Header("Profiles")]
    [SerializeField] private VolumeProfile ambientVolume;
    [SerializeField] private VolumeProfile postProcessVolume;

    [Header("TEMPORARY")]
    [SerializeField] private Light directionalLight;

    private HDAdditionalCameraData mainCamera;

    private void Start()
    {
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<HDAdditionalCameraData>();

#if UNITY_EDITOR

        RenderSettings.shadowQuality = config.shadowQuality;
        RenderSettings.shadows = config.shadows;
        RenderSettings.fog = config.fog;
        RenderSettings.motionBlur = config.motionBlur;
        RenderSettings.depthOfField = config.depthOfField;
        RenderSettings.ambientOcclusion = config.ambientOcclusion;
        RenderSettings.antiAliasing = config.antiAliasing;

#endif

        SetShadow(RenderSettings.shadowQuality, RenderSettings.shadows);
        SetFog(RenderSettings.fog);
        SetMotionBlur(RenderSettings.motionBlur);
        SetDepthOfField(RenderSettings.depthOfField);
        SetAmbientOcclusion(RenderSettings.ambientOcclusion);
        SetAntiAliasing(RenderSettings.antiAliasing);

    }


    #region SETUP COMPONENTS

    private void SetShadow(Quality quality, Enabled enabled)
    {
        switch (quality)
        {
            case Quality.LOW:
                QualitySettings.SetQualityLevel(0);
                break;
            case Quality.MEDIUM:
                QualitySettings.SetQualityLevel(1);
                break;
            case Quality.HIGH:
                QualitySettings.SetQualityLevel(2);
                break;

        }

        if (directionalLight != null)
        {
            if (enabled == Enabled.DISABLED)
                directionalLight.shadows = LightShadows.None;
            else
                directionalLight.shadows = LightShadows.Soft;
        }

    }

    private void SetFog(Enabled active)
    {
        ambientVolume.TryGet(out Fog fog);

        switch (active)
        {
            case Enabled.DISABLED:
                fog.active = false;
                break;
            case Enabled.ENABLED:
                fog.active = true;
                break;
        }

    }

    private void SetMotionBlur(Enabled active)
    {
        postProcessVolume.TryGet<MotionBlur>(out MotionBlur motionBlur);

        switch (active)
        {
            case Enabled.DISABLED:
                motionBlur.active = false;
                break;
            case Enabled.ENABLED:
                motionBlur.active = true;
                break;
        }
    }

    private void SetDepthOfField(Enabled active)
    {
        postProcessVolume.TryGet<DepthOfField>(out DepthOfField depthOfField);

        switch (active)
        {
            case Enabled.DISABLED:
                depthOfField.active = false;
                break;
            case Enabled.ENABLED:
                depthOfField.active = true;
                break;
        }
    }

    private void SetAmbientOcclusion(Enabled active)
    {
        postProcessVolume.TryGet<AmbientOcclusion>(out AmbientOcclusion ambientOcclusion);

        switch (active)
        {
            case Enabled.DISABLED:
                ambientOcclusion.active = false;
                break;
            case Enabled.ENABLED:
                ambientOcclusion.active = true;
                break;
        }
    }

    private void SetAntiAliasing(AntiAliasing antiAliasing)
    {

        if (mainCamera != null)
        {
            switch (antiAliasing)
            {
                case AntiAliasing.FXAA:
                    mainCamera.antialiasing = HDAdditionalCameraData.AntialiasingMode.FastApproximateAntialiasing;
                    break;
                case AntiAliasing.SMAA:
                    mainCamera.antialiasing = HDAdditionalCameraData.AntialiasingMode.SubpixelMorphologicalAntiAliasing;
                    break;
                case AntiAliasing.TAA:
                    mainCamera.antialiasing = HDAdditionalCameraData.AntialiasingMode.TemporalAntialiasing;
                    break;
                case AntiAliasing.DISABLED:
                    mainCamera.antialiasing = HDAdditionalCameraData.AntialiasingMode.None;
                    break;
            }
        }
    }

    #endregion

}
