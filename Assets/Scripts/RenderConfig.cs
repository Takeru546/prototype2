﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Render Config Data", menuName = "Render Manager/Render Config Data", order = 1)]
public class RenderConfig : ScriptableObject
{
    [Header("Settings")]
    public Quality shadowQuality;
    public Enabled fog;
    public Enabled motionBlur;
    public Enabled depthOfField;
    public Enabled ambientOcclusion;
    public AntiAliasing antiAliasing;

    [Header("TEMPORARY")]
    public Enabled shadows;

}
