﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DropdownType
{
    ANTI_ALIASING,
    SHADOWS_QUALITY,
    SHADOWS,
    FOG,
    MOTION_BLUR,
    DOF,
    AMBIENT_OCCLUSION
}

public class HUDSettings : MonoBehaviour
{
    [Header("References")]
    public RenderConfig config;
    [SerializeField] private DropdownManager[] dropdowns;


    private int updatedValue;

    public void ChangeValue(int value, DropdownType type)
    {
        switch (type)
        {
            case DropdownType.ANTI_ALIASING:
                AntiAliasing(value);
                break;
            case DropdownType.SHADOWS_QUALITY:
                ShadowQuality(value);
                break;
            case DropdownType.FOG:
                Fog(value);
                break;
            case DropdownType.MOTION_BLUR:
                MotionBlur(value);
                break;
            case DropdownType.DOF:
                DepthOfField(value);
                break;
            case DropdownType.AMBIENT_OCCLUSION:
                AmbientOcclusion(value);
                break;
            case DropdownType.SHADOWS:
                ShadowsEnabled(value);
                break;
        }

#if UNITY_EDITOR

        UpdateEditorValues();

#endif

    }

    public void UpdateValue()
    {
        for(int i = 0; i < dropdowns.Length; i++)
        {
            GetValue(dropdowns[i].myType);
            dropdowns[i].dropdown.value = updatedValue;
        }
    }

    private void GetValue(DropdownType type)
    {
        switch (type)
        {
            case DropdownType.ANTI_ALIASING:
                updatedValue = (int)RenderSettings.antiAliasing;
                break;
            case DropdownType.SHADOWS_QUALITY:
                updatedValue = (int)RenderSettings.shadowQuality;
                break;
            case DropdownType.FOG:
                updatedValue = (int)RenderSettings.fog;
                break;
            case DropdownType.MOTION_BLUR:
                updatedValue = (int)RenderSettings.motionBlur;
                break;
            case DropdownType.DOF:
                updatedValue = (int)RenderSettings.depthOfField;
                break;
            case DropdownType.AMBIENT_OCCLUSION:
                updatedValue = (int)RenderSettings.ambientOcclusion;
                break;
            case DropdownType.SHADOWS:
                updatedValue = (int)RenderSettings.shadows;
                break;
        }
    }

#if UNITY_EDITOR
    private void UpdateEditorValues()
    {
        config.antiAliasing = RenderSettings.antiAliasing;
        config.shadowQuality = RenderSettings.shadowQuality;
        config.fog = RenderSettings.fog;
        config.motionBlur = RenderSettings.motionBlur;
        config.depthOfField = RenderSettings.depthOfField;
        config.ambientOcclusion = RenderSettings.ambientOcclusion;
        config.shadows = RenderSettings.shadows;

    }

#endif

    #region SETUP

    private void AntiAliasing(int value)
    {
        switch (value)
        {
            case 0:
                RenderSettings.antiAliasing = global::AntiAliasing.FXAA;
                break;
            case 1:
                RenderSettings.antiAliasing = global::AntiAliasing.SMAA;
                break;
            case 2:
                RenderSettings.antiAliasing = global::AntiAliasing.TAA;
                break;
            case 3:
                RenderSettings.antiAliasing = global::AntiAliasing.DISABLED;
                break;
        }

    }

    private void ShadowQuality(int value)
    {
        switch (value)
        {
            case 0:
                RenderSettings.shadowQuality = Quality.LOW;
                break;
            case 1:
                RenderSettings.shadowQuality = Quality.MEDIUM;
                break;
            case 2:
                RenderSettings.shadowQuality = Quality.HIGH;
                break;
        }
    }

    private void Fog(int value)
    {
        switch (value)
        {
            case 0:
                RenderSettings.fog = Enabled.DISABLED;
                break;
            case 1:
                RenderSettings.fog = Enabled.ENABLED;
                break;
        }
    }

    private void MotionBlur(int value)
    {
        switch (value)
        {
            case 0:
                RenderSettings.motionBlur = Enabled.DISABLED;
                break;
            case 1:
                RenderSettings.motionBlur = Enabled.ENABLED;
                break;
        }
    }

    private void DepthOfField(int value)
    {
        switch (value)
        {
            case 0:
                RenderSettings.depthOfField = Enabled.DISABLED;
                break;
            case 1:
                RenderSettings.depthOfField = Enabled.ENABLED;
                break;
        }
    }

    private void AmbientOcclusion(int value)
    {
        switch (value)
        {
            case 0:
                RenderSettings.ambientOcclusion = Enabled.DISABLED;
                break;
            case 1:
                RenderSettings.ambientOcclusion = Enabled.ENABLED;
                break;
        }
    }

    private void ShadowsEnabled(int value)
    {
        switch (value)
        {
            case 0:
                RenderSettings.shadows = Enabled.DISABLED;
                break;
            case 1:
                RenderSettings.shadows = Enabled.ENABLED;
                break;
        }
    }

    #endregion

}
